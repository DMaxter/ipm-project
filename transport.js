// Tipo de transporte
var transportType;
// Data do transporte
var transportDate;
// Iterador para o transporte
var itTransportPlace = 0;
// Iterador para o tempo de notificação
var itTransportTime = 0;

// Timer de atualização de todas as notificações
var allNotifTimer;

// Mensagem caso não haja notificações
const NO_NOTIFICATIONS = "<div id=\"noNotifTitle\"><div>Sem notificações</div></div>";

/* Acertar relógio das notificações */
function transTimeUpdate() {
    var elems;
    var i;

    elems = document.getElementsByClassName("transHour");
    for (i = 0; i < elems.length; i++) {
        elems[i].innerHTML = digitCheck(transportDate.getHours());
    }

    elems = document.getElementsByClassName("transMinute");
    for (i = 0; i < elems.length; i++) {
        elems[i].innerHTML = digitCheck(transportDate.getMinutes());
    }

    elems = document.getElementsByClassName("transDay");
    for (i = 0; i < elems.length; i++) {
        elems[i].innerHTML = digitCheck(transportDate.getDate());
    }

    elems = document.getElementsByClassName("transMonth");
    for (i = 0; i < elems.length; i++) {
        elems[i].innerHTML = months[transportDate.getMonth()];
    }

    elems = document.getElementsByClassName("transYear");
    for (i = 0; i < elems.length; i++) {
        elems[i].innerHTML = transportDate.getFullYear();
    }
}

/* Obter a data atual */
function getTransDate() {
    transportDate = new Date();
    transportDate.setMinutes(transportDate.getMinutes() + 10);
}

/* Escolher o meio de transporte */
function selectTransport(type) {
    transportType = type;
    getTransDate();
    transTimeUpdate();
}

/* Aumentar diminuir valor */
function incDecDate(step, elem) {
    if (step > 0) {
        if (elem.nextSibling.nextSibling.className.search("transHour") != -1) {
            transportDate.setHours(transportDate.getHours() + step);
        } else if (elem.nextSibling.nextSibling.className.search("transMinute") != -1) {
            transportDate.setMinutes(transportDate.getMinutes() + step);
        } else if (elem.nextSibling.nextSibling.className.search("transDay") != -1) {
            transportDate.setDate(transportDate.getDate() + step);
        } else if (elem.nextSibling.nextSibling.className.search("transMonth") != -1) {
            transportDate.setMonth(transportDate.getMonth() + step);
        } else if (elem.nextSibling.nextSibling.className.search("transYear") != -1) {
            transportDate.setYear(transportDate.getYear() + step + 1900);
        }

        /* Impedir uma notificação com idade superior a 10 anos */
        if (transportDate.getFullYear() > new Date().getFullYear() + 10) {
            transportDate.setFullYear(new Date().getFullYear() + 10);
        }
    } else {
        if (elem.previousSibling.previousSibling.className.search("transHour") != -1) {
            transportDate.setHours(transportDate.getHours() + step);
        } else if (elem.previousSibling.previousSibling.className.search("transMinute") != -1) {
            transportDate.setMinutes(transportDate.getMinutes() + step);
        } else if (elem.previousSibling.previousSibling.className.search("transDay") != -1) {
            transportDate.setDate(transportDate.getDate() + step);
        } else if (elem.previousSibling.previousSibling.className.search("transMonth") != -1) {
            transportDate.setMonth(transportDate.getMonth() + step);
        } else if (elem.previousSibling.previousSibling.className.search("transYear") != -1) {
            transportDate.setFullYear(transportDate.getFullYear() + step);
        }

        /* Impedir selecionar uma data "impossível" (inferior a 10min a menos da atual) */
        if (transportDate.getTime() < new Date().getTime() + 10 * 60 * 1000) {
            getTransDate();
        }
    }

    transTimeUpdate();
}

/* Atualizar o local */
function updateTransPlace() {
    document.getElementById("notTransPlace").innerHTML = document.getElementById("transPlace").innerHTML = Transports[transportType][itTransportPlace];
}

/* Alterar o local de embarque */
function changeTransPlace(step) {
    itTransportPlace = (itTransportPlace + step).mod(Transports[transportType].length);
    updateTransPlace();
}

/* Resetar escolhas */
function resTrans() {
    itTransportPlace = 0;
    itTransportTime = 0;
    transportType = 0;
    getTransDate();
}

/* Alterar tempo de notificação */
function changeNotifyTime(step) {
    itTransportTime += step;

    // Prevenir sair fora dos limites
    if (itTransportTime < 0) {
        itTransportTime = 0;
    } else if (itTransportTime == NotificationsTime.length) {
        itTransportTime = NotificationsTime.length - 1;
    }

    // Impedir notificação antes da hora atual
    var date = new Date();
    date.setTime(transportDate.getTime() - NotificationsTime[itTransportTime]["time"] * 1000);
    if (transportDate.getTime() < Date.now()) {
        transportDate.setTime(Date.now() + 10 * 60 * 1000);
    } else if (date.getTime() < Date.now()) {
        itTransportTime -= step;
    }

    updateTransTime();
}

/* Atualizar tempo de notificação */
function updateTransTime() {
    document.getElementById("notTransTime").innerHTML = document.getElementById("transNotTime").innerHTML = NotificationsTime[itTransportTime]["tag"];
}

/* Criar notificação */
function createNotification() {
    var notifTime = new Date();
    // Calcular a hora de notificação
    notifTime.setTime(transportDate.getTime() - NotificationsTime[itTransportTime]["time"] * 1000);

    Notifications.push({
        "type": transportType,
        "leaveTime": transportDate,
        "notIterator": itTransportTime,
        "place": itTransportPlace,
        "notified": false,
        "notTime": notifTime,
        "id": makeID(10),
    });

    // Ordenar por hora de embarque
    Notifications.sort(compareNotifs);

    // Fazer reset às escolhas
    resTrans();
}

/* Ordenação de notificações por hora de embarque */
function compareNotifs(a, b) {
    if (a["leaveTime"].getTime() <= b["leaveTime"].getTime()) {
        return -1;
    } else {
        return 1;
    }
}

/* Verificar se está na hora de notificar */
function checkNotify() {
    var now = new Date();

    var i;
    for (i = 0; i < Notifications.length; i++) {
        // Notificar
        if (Notifications[i]["notTime"].getTime() < now.getTime() &&
            Notifications[i]["notified"] == false) {
            notify(Notifications[i]);
            // Eliminar notificações passadas
        } else if (Notifications[i]["leaveTime"].getTime() < now.getTime()) {
            Notifications.splice(i, 1);
        }
    }

    // Verificar se colocou alguma
    if (i == 0) {
        notify("NO");
    }

    // Tentar de minuto a minuto
    setTimeout(checkNotify, 60000);
}

/* Notificar */
function notify(notification) {
    var notifBox = document.getElementById("notifBox");
    var HTML = "";

    // Verificar se é para colocar notificação
    if (notification != "NO") {
        if (notifBox.contains(document.getElementById("noNotifTitle"))) {
            notifBox.innerHTML = "";
        }

        HTML = "<div class=\"disNotifItem\" onclick=\"removeNotif(this)\">" + generateNotification(notification) + "</div>";
        notification["notified"] = true;
		stopLight();
		blinkLight();
    } else {
        // Apenas colocar mensagem se não houver notificações
        if (notifBox.childElementCount == 0) {
            HTML = NO_NOTIFICATIONS;
        }
    }

    notifBox.innerHTML += HTML;
}

/* Escolher ícone da notificação */
function chooseNotifIcon(icon) {
    switch (icon) {
        case "Bus":
            return "busIcon";
        case "Train":
            return "trainIcon";
        case "Plane":
            return "planeIcon";
        case "Boat":
            return "boatIcon";
    }
}

/* Transformar data completa em string */
function fullDateToText(time) {
    return digitCheck(time.getHours()) + ":" + digitCheck(time.getMinutes()) + " " + time.getDate() + " " + months[time.getMonth()] + " " + time.getFullYear();
}

/* Criar HTML da notificação */
function generateNotification(notification) {
    var icon = chooseNotifIcon(notification["type"]);
    var text = fullDateToText(notification["leaveTime"]);;

    return "<div class=\"notif\"><div class=\"icon transpIcon " + icon + "\"></div><div class=\"notifTitle\">" + text + "</div></div>";
}

/* Criar HTML para ecrã de todas as notificações */
function generateAllNotification(notification) {
    return "<div id=\"" + notification["id"] + "\" class=\"notifItem\" onclick=\"editNotif(searchNotif(this));\">" + generateNotification(notification) + "</div>";
}

/* Atualizar lista de todas as notificações */
function updateAllNotif() {
    clearTimeout(allNotifTimer);

    var all = document.getElementById("allNotifs");

    // Limpar tudo
    all.innerHTML = "";

    var i;
    for (i = 0; i < Notifications.length; i++) {
        all.innerHTML += generateAllNotification(Notifications[i]);
    }

    // Se não houver notificações
    if (i == 0) {
        all.innerHTML = NO_NOTIFICATIONS;
    }

    // Prevenir editar notificação passada
    allNotifTimer = setTimeout(updateAllNotif, 60000);
}

/* Remover notificação */
function removeNotif(elem) {
    elem.parentNode.removeChild(elem);
}

/* Eliminar notificação */
function deleteNotif(elem) {
    var i;
    for (i = 0; i < Notifications.length; i++) {
        if (Notifications[i]["id"] == elem) {
            Notifications.splice(i, 1);
            return;
        }
    }
}

/* Procurar notificação */
function searchNotif(notif) {
    var i;
    for (i = 0; i < Notifications.length; i++) {
        if (Notifications[i]["id"] == notif.id) {
            return Notifications[i];
        }
    }
}

/* Editar notificação */
function editNotif(notification) {
    changeScreen("edNotif1");

    // Colocar notificação
    updateEdNotif(notification);

    // Colocar valores nas variáveis globais
    transportType = notification["type"];
    transportDate = notification["leaveTime"];
    itTransportTime = notification["notIterator"];
    itTransportPlace = notification["place"];

    // Editar transporte
    document.getElementById("notifIcon").addEventListener("click", function() {
        changeScreen("edNotif2");

        // Preparar informações para o próximo ecrã
        let icons = document.querySelectorAll("#edNotif2 .transpIcon");

        var i;
        for (i = 0; i < icons.length; i++) {
            icons[i].addEventListener("click", function() {
                changeScreen("edNotif3");
                updateTransTime();
                updateTransPlace();

                document.querySelector("#edNotif3 .backIcon").addEventListener("click", function() {
                    changeScreen("edNotif2");
                });
            });
        }
    });

    // Editar local e hora de notificação
    document.getElementById("notifTime").onclick = document.getElementById("notifPlace").onclick = function() {
        changeScreen("edNotif3");

        document.querySelector("#edNotif3 .backIcon").addEventListener("click", function() {
            changeScreen("edNotif1");
        });
    };

    // Clique ícone de check
    document.querySelector("#edNotif3 .confIcon").addEventListener("click", function() {
        // Calcular a hora de notificação
        var notifTime = new Date();
        notifTime.setTime(transportDate.getTime() - NotificationsTime[itTransportTime]["time"] * 1000);

        // Guardar valores
        notification["type"] = transportType;
        notification["leaveTime"] = transportDate;
        notification["notIterator"] = itTransportTime;
        notification["place"] = itTransportPlace;
        notification["notTime"] = notifTime;

        // Atualizar notificação
        updateEdNotif(notification);
        Notifications.sort(compareNotifs);
    });


    // Editar data de partida
    document.getElementById("notifDate").addEventListener("click", function() {
        changeScreen("edNotif4");
    });

    // Avançar no ecrã de edição da data
    document.querySelector("#edNotif4 .nextIcon").addEventListener("click", function() {
        document.querySelector("#edNotif3 .backIcon").addEventListener("click", function() {
            changeScreen("edNotif4");
        });
    });

    // Clique para ecrã de eliminação
    document.getElementById("delNotif").addEventListener("click", function() {
        changeScreen("delNotification");
    });

    // Botão "Sim" no ecrã de eliminação
    document.getElementById("yDelNotif").addEventListener("click", function() {
        changeScreen("transpNotif");
        deleteNotif(notification["id"]);
        updateAllNotif();
    });

    // Botão "Não" no ecrã de eliminação
    document.getElementById("nDelNotif").addEventListener("click", function() {
        changeScreen("edNotif1");
    });
}

/* Atualizar ecrã de edição */
function updateEdNotif(notif) {
    var icon = chooseNotifIcon(notif["type"]);
    var text = fullDateToText(notif["leaveTime"]);

    document.getElementById("notifIcon").className = "icon transpIcon " + icon;
    document.getElementById("notifDate").innerHTML = text;
    document.getElementById("notifTime").innerHTML = NotificationsTime[notif["notIterator"]]["tag"];
    document.getElementById("notifPlace").innerHTML = Transports[notif["type"]][notif["place"]];
}
