// Número do cartão
var cardNumber = [];
// Identificar empresa do cartão
var cardCompany;
// Validade do cartão
var cardVal;
// Iterador do número no cartão
var cardNumberIt = 0;
// Índice do cartão a remover
var remCardInd;

// Teclas do teclado numérico
var keys;

// Mensagem caso não haja cartões associados
const NO_CARDS = "<div id=\"noCardsMsg\">Sem cartões</div>";
// Tamanho máximo do cartão
const MAX_CARD_SIZE = 16;

window.addEventListener("load", function(){
	// Funções das teclas
	keys = document.querySelectorAll("#cardNumber .numKey");
	updateKeys();

	// Colocar mensagem sem cartão
	document.querySelector("#payments #cards").innerHTML = NO_CARDS;

	document.querySelector("#confirmCard .confirmIcon").addEventListener("click", function(){
		// Criar cartão
		Cards.push({
			"number": fourDigCard(cardNumber),
			"valid": cardVal,
			"company": cardCompany
		});

		// Limpar valores
		resCard();

		// Atualizar ecrã
		updateNewCard();
		updateCards();
	});
});

/* Inicializar adição */
function initPay(){
	document.querySelector("#cardNumber .backIcon").onclick = function(){
		changeScreen("payments");
		simulCardOFF();
	}

	document.querySelector("#confirmCard .confirmIcon").onclick = function(){
		changeScreen("payments");
	}
}

// Atualizar cartões
function updateCards(){
	var i;
	var HTML = "";
	for(i = 0; i < Cards.length; i++){
		HTML += genCardHTML(Cards[i]);
	}

	if(i == 0){
		document.querySelector("#payments #cards").innerHTML = "<div id=\"noCardsMsg\">Sem cartões</div>";
	}else{
		document.querySelector("#payments #cards").innerHTML = HTML;
	}

	// Função para remover cartão
	var cards = document.querySelector("#cards").children;

	for(let i = 0; i < cards.length; i++){
		var ham = new Hammer(cards[i]);
		ham.on("press", function(event){
			changeScreen("delCard");
			delCard(event.target);
		});
	}
}

// Funcionalidade das teclas
function updateKeys(){
	var next = document.querySelector("#cardNumber .nextIcon");

	for(let i = 0; i < keys.length; i++){
		switch(i){
			case 0:
			case 1:
			case 2:
			case 3:
			case 4:
			case 5:
			case 6:
			case 7:
			case 8:
			case 9: keys[i].addEventListener("click", async function(){
						if(cardNumberIt < MAX_CARD_SIZE){
							cardNumber[cardNumberIt] = i.toString();
							cardNumberIt++;
							updateNewCard();
						}

						// Desbloquear botão de avançar
						if(cardNumberIt == MAX_CARD_SIZE){
							if(inCards(fourDigCard(cardNumber))){
								document.getElementById("newCardNumber").innerHTML = "Cartão existente";
							}else if(checkCard(cardNumber.join(""))){
								next.style.display = "initial";
							}else{
								document.getElementById("newCardNumber").innerHTML = "Cartão inválido";
							}
							await sleep(3000);
							updateNewCard();
						}
					}); break;
				// Tecla de limpar 1 caracter
			case 10: keys[i].addEventListener("click", function(){
						if(cardNumberIt != 0){
							cardNumberIt--;
							cardNumber[cardNumberIt] = " ";
							updateNewCard();
						}

						next.style.display = "none";

					}); break;
				// Tecla de limpar tela
			case 11: keys[i].addEventListener("click", function(){
						for(let j = cardNumberIt - 1; j >= 0; j--){
							cardNumber[j] = " ";
						}

						next.style.display = "none";

						cardNumberIt = 0;
						updateNewCard();
					}); break;
		}
	}

	document.getElementById("cardNumBack").addEventListener("click", function(){
		for(let j = cardNumberIt - 1; j >= 0; j--){
							cardNumber[j] = " ";
						}

						next.style.display = "none";

						cardNumberIt = 0;
						updateNewCard();
	});
}

// Apresentar cartão 4 digitos
function fourDigCard(card){
	var text = "";
	for(let i = 0, j = 0; j < cardNumberIt; i++){
		if(i % 5 != 4){
			text += cardNumber[j++];
		}else{
			text += " ";
		}
	}

	return text;
}

// Atualizar número do cartão
function updateNewCard(){
	var cardScreen = document.getElementById("newCardNumber");

	cardScreen.innerHTML = fourDigCard(cardNumber);
}

// Obter data atual
function getValDate(){
	cardVal = new Date();
	cardVal.setMonth(cardVal.getMonth() + 1);
	cardVal.setDate(0);
}

// Alterar data de validade do cartão
function incDecVal(step, elem){
	if(step > 0){
		if(elem.nextSibling.nextSibling.id == "valMonth"){
			cardVal.setDate(1);
			cardVal.setMonth(cardVal.getMonth() + step + 1);
			cardVal.setDate(0);
		}else if(elem.nextSibling.nextSibling.id == "valYear"){
			cardVal.setFullYear(cardVal.getFullYear() + step);
		}
	}else{
		if(elem.previousSibling.previousSibling.id == "valMonth"){
			cardVal.setMonth(cardVal.getMonth() + step + 1);
			cardVal.setDate(0);
		}else if(elem.previousSibling.previousSibling.id == "valYear"){
			cardVal.setFullYear(cardVal.getFullYear() + step);
		}
	}

	if(cardVal.getFullYear() > new Date().getFullYear() + 5 || (cardVal.getFullYear() == new Date().getFullYear() + 5 && cardVal.getMonth() > new Date().getMonth())){
		getValDate();
		cardVal.setFullYear(cardVal.getFullYear() + 5);
	}else if(cardVal.getTime() < Date.now()){
		getValDate();
	}

	updateValDate();
}

// Atualizar validade do cartão
function updateValDate(){
	document.getElementById("valMonth").innerHTML = digitCheck(cardVal.getMonth() + 1);
	document.getElementById("valYear").innerHTML = cardVal.getFullYear();
}

// Verificar cartão de crédito
function checkCard(card) {
    return card.split('')
        .reverse()
        .map( (x) => parseInt(x) )
        .map( (x,idx) => idx % 2 ? x * 2 : x )
        .map( (x) => x > 9 ? (x % 10) + 1 : x )
        .reduce( (accum, x) => accum += x ) % 10 === 0;
}

// Resetar os valores
function resCard(){
	cardCompany = "";
	cardNumber = [];
	cardNumberIt = 0;

	document.querySelector("#cardNumber .nextIcon").style.display = "none";
}

// Gerar HTML cartão
function genCardHTML(card){
	return "<div class=\"card\"id=\"" + card["number"] + "\" onclick=\"payScrn(this); simulCardOFF();\"><div class=\"icon cardIcon "+ card["company"] + "\"></div><div class=\"cardNumber\">" + card["number"] + "</div></div>";
}

// Confirmar cartão
function confirmCard(){
	document.querySelector("#confirmCard #checkNum").innerHTML = fourDigCard(cardNumber);
	document.querySelector("#confirmCard #checkMonth").innerHTML = digitCheck(cardVal.getMonth() + 1);
	document.querySelector("#confirmCard #checkYear").innerHTML = cardVal.getFullYear();;

	// Selecionar empresa do cartão
	if(cardNumber[0] == "4"){
		cardCompany = "Visa";
	}else if(cardNumber[0] == "5"){
		cardCompany = "MasterCard";
	}else{
		cardCompany = "Desconhecido";
	}
}

// Ecrã de pagamento
function payScrn(card){
	document.getElementById("payMsg").innerHTML = "<div>Encoste o iGo ao terminal de pagamento</div>";
		
	changeScreen("pay1");
	document.getElementById("simulPay").style.display = "grid";

	document.querySelector("#pay1 .backIcon").addEventListener("click", function(){
		changeScreen("payments");
		document.getElementById("simulPay").style.display = "none";
	});

	document.querySelector("#pay1 .switchIcon").addEventListener("click", function(){
		changeScreen("payments");
		document.getElementById("simulPay").style.display = "none";
	});
}

// Encontrar cartão
function inCards(card){
	for(let i = 0; i < Cards.length; i++){
		if(Cards[i]["number"] == card){
			return true;
		}
	}

	return false;
}

// Gerar HTML simples
function genSCardHTML(card){
	return "<div class=\"scard\"><div class=\"icon cardIcon "+ card["company"] + "\"></div><div class=\"cardNumber\">" + card["number"] + "</div></div>";
}

// Gerar HTML cartão para PI
function genPICardHTML(card){
	return "<div class=\"card\"id=\"" + card["number"] + "\" onclick=\"payScrnPI(this);\"><div class=\"icon cardIcon "+ card["company"] + "\"></div><div class=\"cardNumber\">" + card["number"] + "</div></div>";
}

// Ecrã de pagamento para PI
function payScrnPIInner(card){
	changeScreen("pay1PI");
	document.querySelector("#pay1PI #piNumPay").innerHTML = piNum + " entradas - Total: " + piPrice + "€\nCartão: " + card["number"] + "\nConfirmar pagamento?";
}

function payScrnPI(card){
	for(let i = 0; i < Cards.length; i++){
		if(card.id == Cards[i]["number"]){
			payScrnPIInner(Cards[i]);
		}
	}
}

// Eliminar cartão
function delCard(elem){
	var card;

	if(elem.id == ""){
		elem = elem.parentNode;
	}

	for(let i = 0; i < Cards.length; i++){
		if(Cards[i]["number"] == elem.id){
			card = Cards[i];
			remCardInd = i;
			break;
		}
	}

	document.querySelector("#delCard #confirmDeleteCard").innerHTML = genSCardHTML(card);

	document.querySelector("#delCard .trashIcon").addEventListener("click", function(){
		Cards.splice(remCardInd, 1);
		changeScreen("payments");
		updateCards();
	});
}

// Simulação de cartão
async function simulCard(type){
	switch(type){
		case "visa": cardNumber = "4262168218707263".split(""); break;
		case "master": cardNumber = "5109710836838006".split(""); break;
		case "wrong": cardNumber = "3529375359232668".split(""); break;
		default: cardNumber = "3529375359232669".split(""); break;
	}

	cardNumberIt = 16;
	updateNewCard();

	if(cardNumberIt == MAX_CARD_SIZE){
		if(inCards(fourDigCard(cardNumber))){
			document.querySelector("#cardNumber .nextIcon").style.display = "none";
			document.getElementById("newCardNumber").innerHTML = "Cartão existente";
		}else if(checkCard(cardNumber.join(""))){
			document.querySelector("#cardNumber .nextIcon").style.display = "initial";
		}else{
			document.querySelector("#cardNumber .nextIcon").style.display = "none";
			document.getElementById("newCardNumber").innerHTML = "Cartão inválido";
		}

		await sleep(3000);
		updateNewCard();
	}
}

function simulCardON(){
	document.querySelector("#simulCard").style.display = "grid";
}

function simulCardOFF(){
	document.querySelector("#simulCard").style.display = "none";
}

// Simulação de pagamento
async function pay(){
	// Falhar pagamento
	if(Math.random() < 0.5){
		document.getElementById("payMsg").innerHTML = "<div class=\"icon cancelledIcon\"></div><div>O pagamento falhou</div>";
		document.getElementById("simulPay").style.display = "none";
		document.querySelector("#pay1 .switchIcon").style.display = "none";
		document.querySelector("#pay1").addEventListener("click", remConfErr);

	// Concluir pagamento
	}else{
		document.getElementById("payMsg").innerHTML = "<div class=\"icon verifiedIcon\"></div><div>Pagamento bem sucedido</div>";
		document.getElementById("simulPay").style.display = "none";
		document.querySelector("#pay1 .switchIcon").style.display = "none";
		document.querySelector("#pay1").addEventListener("click", remConfSuc);
	}
}

// Simulação pagamento de transporte
function simulTranspPay(){
	var n = Math.round(Math.random() * 4);

	switch(n){
		case 0: transportType = "Boat"; break;
		case 1: transportType = "Plane"; break;
		case 2: transportType = "Train"; break;
		case 3: transportType = "Bus"; break;
	}

	getTransDate();

	itTransportPlace = Math.round(Math.random() * (Transports[transportType].length - 1));

	createNotification();

	document.getElementById("payMsg").innerHTML = "<div class=\"icon verifiedIcon\"></div><div>Pagamento bem sucedido e notificação adicionada</div>";
	document.getElementById("simulPay").style.display = "none";
	document.querySelector("#pay1 .switchIcon").style.display = "none";

	updateAllNotif();

	document.querySelector("#pay1").addEventListener("click", remConfSuc);
}

// Remover mensagem de confirmação
function remConfErr(){
	document.querySelector("#pay1 .switchIcon").style.display = "initial";
	document.getElementById("simulPay").style.display = "grid";
	document.getElementById("payMsg").innerHTML = "<div>Encoste o iGo ao terminal de pagamento</div>";
	document.querySelector("#pay1").removeEventListener("click", remConfErr);
}

function remConfSuc(){
	document.querySelector("#pay1 .switchIcon").style.display = "initial";
	changeScreen("payments");
	document.querySelector("#pay1").removeEventListener("click", remConfSuc);
}
