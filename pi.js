/* Escolha da categoria de PI */
var piChoice;

/* Preço total das entradas */
var piPrice;

/* Quantidade de entradas */
var piNum;

/* Caminho atual a ser mostrado */
var curPath;

/* Lugares (ruas, praças, travessas) */
const PLACES_SIZE = Places.length;

// Mensagem caso não haja PIs
const NO_PI = "<div id=\"noPIMsg\"><div>Sem pontos de inter.</div></div>";

// Mensagem de chegada ao destino
const FIN_DEST = "<div class=\"direction\"><div class=\"icon dirIcon endPathIcon\"></div><div class=\"dirText\">Chegou ao seu destino</div></div>";

window.addEventListener("load", function(){
	// Gerar os caminhos para todos os PI
	for(var cat in PI){
		for(let i = 0; i < PI[cat].length; i++){
			var [path, dist] = genPath();

			path.push(FIN_DEST);

			PI[cat][i]["distance"] = dist;
			PI[cat][i]["path"] = path;
		}
	}
	// Colocar mensagem sem PIs
	document.getElementById("myPI").innerHTML = NO_PI;

	document.querySelector("#piBuy .nextIcon").addEventListener("click", function(){
		updateSelCard();
	});
});

/* Escolher o PI */
function choosePi(cat){
	piChoice = cat;
	showClosePlaces();
}

/* Mostrar os locais */
function showClosePlaces(){
	var icon = getIcon(piChoice);

	places = document.querySelector("#piClosePlaces #allLocations");

	places.innerHTML = "";

	for(let i = 0; i < PI[piChoice].length; i++){
		if(PI[piChoice][i]["visible"]){
			places.innerHTML += genPlaceContHTML(PI[piChoice][i]["name"], distUnit(PI[piChoice][i]["distance"]), icon, piChoice + "-" + i);
		}
	}
}

/* Calcular a distância corretamente */
function distUnit(dist){
	if(dist < 1000){
		return "" + dist + " m";
	}else{
		return "" + (dist/1000).toFixed(2) + " km";
	}
}

/* Obter ícone */
function getIcon(choice){
	switch(choice){
		case "Restaurants": return "restIcon";
		case "Libraries": return "libIcon";
		case "Parks": return "parkIcon";
		case "Galleries": return "galIcon";
	}
}

/* Adicionar local */
function addPI(pi){
	var piData = pi.id.split("-");

	MyPI.push({
		"index": piData[1],
		"category": piData[0],
		"icon": getIcon(piData[0]),
		"name": PI[piData[0]][piData[1]]["name"],
		"distance": PI[piData[0]][piData[1]]["distance"],
		"path": PI[piData[0]][piData[1]]["path"],
		"price": PI[piData[0]][piData[1]]["price"],
		"ID": makeID(20)
	});

	/* "Remover" da lista de escolhas */
	PI[piData[0]][piData[1]]["visible"] = false;

	/* Ordenar por distância */
	MyPI.sort(compPI);

	updatePI();

	changeScreen("pi");
}

/* Remover local */
function removePI(pi){
	if(pi.id == ""){
		pi = pi.parentNode;
	}

	var ind = findMyPIInd(pi.id);
	var elem = MyPI[ind];

	document.querySelector("#piRemove #confirmDeletePI").innerHTML = genPlaceContHTML(elem["name"], distUnit(elem["distance"]), elem["icon"], elem["id"]);

	document.querySelector("#piRemove .trashIcon").onclick = function(){
		MyPI.splice(ind, 1);
		changeScreen("pi");
		PI[elem["category"]][elem["index"]]["visible"] = true;
		updatePI();
	};

	changeScreen("piRemove");
}

/* Mostrar caminho */
function showPath(pi){
	showPathInner(pi.id);
}

function showPathInner(pi){
	var elem = curPath = findMyPI(pi);

	// Colocar título
	document.querySelector("#piPath .title").innerHTML = elem["name"];

	// Colocar caminho
	document.querySelector("#piPath #myPIPath").innerHTML = elem["path"].join('');

	if(curPath["price"] == 0){
		document.querySelector("#piPath .buyIcon").style.display = "none";
	}else{
		document.querySelector("#piPath .buyIcon").style.display = "initial";
	}

	document.querySelector("#piPath .buyIcon").onclick = function(){
		buyEntrance(elem["price"]);
		simulMovOFF();
		changeScreen("piBuy");
	};

	changeScreen("piPath");
}

/* Comprar entradas */
function buyEntrance(price){
	var quantity = 1;

	document.querySelector("#piBuy #entranceUnitPrice").innerHTML = price.toFixed(2) + "€";
	updatePrice(1, price);

	document.querySelector("#piBuy .minusBtn").onclick = function(){
		quantity--;

		if(quantity < 1){
			quantity = 1;
		}

		updatePrice(quantity, price);
	};

	document.querySelector("#piBuy .plusBtn").onclick = function(){
		quantity++;

		if(quantity == 11){
			changeScreen("confirmPlus10");
		}

		updatePrice(quantity, price);
	};

	document.querySelector("#piBuy .nextIcon").onclick = function(){
		changeScreen("piCard");
	};

	document.querySelector("#confirmPlus10 .yesBtn").onclick = function(){
		changeScreen("piBuy");
	}

	document.querySelector("#confirmPlus10 .noBtn").onclick = function(){
		quantity = 10;
		updatePrice(quantity, price);
		changeScreen("piBuy");
	}
}

/* Atualizar preço */
function updatePrice(amount, price){
	document.querySelector("#piBuy #entranceNum").innerHTML = amount;
	document.querySelector("#piBuy #entrancePrice").innerHTML = (amount * price).toFixed(2) + "€";
	piNum = amount;
	piPrice = (amount * price).toFixed(2);
}

/* Atualizar lista de cartões */
function updateSelCard(){
	var elem = document.querySelector("#piCard #selCard");
	var curCard = -1;

	var i;
	var HTML = "";
	for(i = 0; i < Cards.length; i++){
		HTML += genPICardHTML(Cards[i]);
	}

	if(i == 0){
		elem.innerHTML = "<div id=\"noCardsMsg\">Sem cartões</div>";
	}else{
		elem.innerHTML = HTML;
	}
}

/* Adicionar cartão através do PI */
function addPICard(){
	getValDate();
	simulCardON();
	changeScreen("cardNumber");

	/* Redefinir andar para trás */
	document.querySelector("#cardNumber .backIcon").onclick = function(){
		simulCardOFF();
		changeScreen("piCard");
	}

	/* Redefinir terminar adição */
	document.querySelector("#confirmCard .confirmIcon").onclick = function(){
		payScrnPIInner(Cards[Cards.length - 1]);
	}
}

/* Atualizar ecrã dos pontos de interesse */
function updatePI(){
	var elem = document.getElementById("myPI");

	elem.innerHTML = "";

	var i;
	for(i = 0; i < MyPI.length; i++){
		elem.innerHTML += genMyHTML(MyPI[i]["name"], distUnit(MyPI[i]["distance"]), MyPI[i]["icon"], MyPI[i]["ID"]);
	}

	// Se não houver PIs
    if (i == 0) {
        elem.innerHTML = NO_PI;
    }

	// Função para remover PI
	var pis = document.querySelector("#myPI").children;

	for(let i = 0; i < pis.length; i++){
		var ham = new Hammer(pis[i]);
		ham.on("press", function(event){
			removePI(event.target);
		});
	}
}

/* Gerador de caminhos */
function genPath(){
	// 3 a 15 passos
	var steps = Math.round(Math.random() * 12) + 3;

	var HTML = [];
	var distance = 0;

	for(let i = 0; i < steps; i++){
		var dist = chooseDist();
		distance += dist;

		var val = Math.round(Math.random() * PLACES_SIZE);

		HTML.push(genDirection(Places[val], dist));
	}

	return [HTML, distance];
}

function chooseDist(){
	var val = Math.round(Math.random() * 10);

	switch(val){
		case 0: return 5;
		case 1: return 10;
		case 2: return 20;
		case 3: return 30;
		case 4: return 50;
		case 5: return 75;
		case 6: return 100;
		case 7: return 150;
		case 8: return 250;
		case 9: return 400;
		case 10: return 500;
	}
}

/* Encontrar PI pelo ID */
function findMyPI(pi){
	for(let i = 0; i < MyPI.length; i++){
		if(MyPI[i]["ID"] == pi){
			return MyPI[i];
		}
	}
}

function findMyPIInd(pi){
	for(let i = 0; i < MyPI.length; i++){
		if(MyPI[i]["ID"] == pi){
			return i;
		}
	}
}

/* Comparar a distância entre 2 PI */
function compPI(a, b){
	return a["distance"] - b["distance"];
}

/* Geradores de HTML */

function genPlaceHTML(place, dist){
	return "<div class=\"piTitle\"><div>" + place + "</div><div class=\"piDist\">a " + dist + "</div></div>";
}

function genDistHTML(dist){
	return "<div class\"piDist\">" + dist + "</div>";
}

/* Gerador de PI */
function genPlaceContHTML(place, dist, icon, id){
	return "<div class=\"pi\" id=\"" + id + "\" onclick=\"addPI(this);\"><div class=\"icon piIcon " + icon + "\"></div>" + genPlaceHTML(place, dist) + "</div>";
}

/* Gerador de myPI */
function genMyHTML(place, dist, icon, id){
	return "<div class=\"pi\" id=\"" + id + "\" onclick=\"showPath(this); simulMovON()\"><div class=\"icon piIcon " + icon + "\"></div>" + genPlaceHTML(place, dist) + "</div>";
}

function genDirection(street, dist){
	var val = Math.round(Math.random() * 6);
	var dir;

	switch(val){
		case 0: dir = "turnLeft"; break;
		case 1: dir = "turnLeftFront"; break;
		case 2: dir = "turnLeftBack"; break;
		case 3: dir = "turnRight"; break;
		case 4: dir = "turnRightFront"; break;
		case 5: dir = "turnRightBack"; break;
		case 6: dir = "straightAhead"; break;
	}

	return "<div class=\"direction\"><div class=\"icon dirIcon " + dir + "\"></div><div class=\"dirText\">A " + dist + "m " + dirText[dir] + street + "</div></div>";
}

/* Simular movimento */
function simulMov(){
	if(curPath["path"].length != 1){
		var less = curPath["path"][0].match(/\d+/g);

		curPath["distance"] -= Number(less);
		curPath["path"].shift();
		showPathInner(curPath["ID"]);
	}
}

function simulMovON(){
	document.getElementById("simulMov").style.display = "grid";
}

function simulMovOFF(){
	document.getElementById("simulMov").style.display = "none";
}
