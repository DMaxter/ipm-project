var PI = {
    "Restaurants": [{
		"name": "Tasca do Zé",
		"distance": 0,
		"visible": true,
		"path": [],
		"price": 0
	},{
        "name": "Pizzaria CasaNova",
        "distance": 0,
        "visible": true,
        "path": [],
        "price": 0
    }],
    "Libraries": [{
		"name": "Biblioteca Municipal",
		"distance": 0,
		"visible": true,
		"path": [],
		"price": 3.5
	},{
        "name": "Biblioteca de Marvila",
        "distance": 0,
        "visible": true,
        "path": [],
        "price": 2
    },{
        "name": "Biblioteca CCB",
        "distance": 0,
        "visible": true,
        "path": [],
        "price": 1
    }],
    "Parks": [{
		"name": "Parque da Boa Vista",
		"distance": 0,
		"visible": true,
		"path": [],
		"price": 3
	},{
        "name": "Parque Eduardo VII",
        "distance": 0,
        "visible": true,
        "path": [],
        "price": 0
    }],
    "Galleries": [{
		"name": "Galeria Ás",
		"distance": 0,
		"visible": true,
		"path": [],
		"price": 10
	}]
};

var dirText = {
	"turnLeft": "vire à esquerda para a ",
	"turnLeftFront": "vire ligeiramente à esquerda para a ",
	"turnLeftBack": "vire à esquerda para a ",
	"turnRight": "vire à direita para a ",
	"turnRightFront": "vire ligeiramente à direita para a ",
	"turnRightBack": "vire à direita para a ",
	"straightAhead": "siga em frente para a "
}

var Transports = {
    "Boat": [
        "Porto Lisboa",
        "Porto Aveiro",
        "Porto Leixões",
        "Porto V. Castelo",
        "Porto Setúbal",
        "Porto Douro"
    ],
    "Plane": [
        "Aeroporto Lisboa",
        "Aeroporto Faro",
        "Aeroporto Porto",
        "Aeroporto Madeira",
        "Aeroporto P. Santo",
        "Aeroporto Beja",
        "Aeroporto P. Delgada",
        "Aeroporto Sta. Maria",
        "Aeroporto Horta"
    ],
    "Train": [
        "Estação Pinhão",
        "Estação São Bento",
        "Estação Aveiro",
        "Estação Vil. Formoso",
        "Estação Coimbra",
        "Estação Rossio",
        "Gare Oriente",
        "Estação Sta. Apolónia"
    ],
    "Bus": [
        "Terminal Sete Rios",
        "Terminal G. Oriente",
        "Terminal Leiria",
        "Terminal Faro",
        "Terminal Viseu",
        "Terminal Braga",
        "Terminal Aveiro",
        "Terminal Cast. Branco",
        "Terminal Porto"
    ]
};

var NotificationsTime = [{
        "tag": "5 minutos antes",
        "time": 5 * 60,
    },
    {
        "tag": "10 minutos antes",
        "time": 10 * 60,
    },
    {
        "tag": "15 minutos antes",
        "time": 15 * 60,
    },
    {
        "tag": "20 minutos antes",
        "time": 20 * 60,
    },
    {
        "tag": "30 minutos antes",
        "time": 30 * 60,
    },
    {
        "tag": "45 minutos antes",
        "time": 45 * 60,
    },
    {
        "tag": "1 hora antes",
        "time": 60 * 60,
    },
    {
        "tag": "2 horas antes",
        "time": 2 * 60 * 60,
    },
    {
        "tag": "5 horas antes",
        "time": 5 * 60 * 60,
    },
    {
        "tag": "10 horas antes",
        "time": 10 * 60 * 60,
    },
    {
        "tag": "1 dia antes",
        "time": 24 * 60 * 60,
    }
];

var helpTexts = {
    "home": "Selecione uma funcionalidade<p>Duplo clique no botão de bloqueio para voltar para o menu inicial</p><p>Quando o LED piscar, arraste para baixo em qualquer ecrã para aceder às notificações</p><p>Para as eliminar clique nelas e para voltar ao menu inicial arraste para cima</p>",
    "transpNotif": "Selecione a notificação para aceder ao seu menu de edição ou crie uma nova",
    "transpCategories": "Selecione o meio de transporte",
    "transpTime": "Introduza a data de embarque",
    "transpStation": "Selecione o local de embarque e o intervalo de tempo com que deseja ser notificado",
    "edNotif1": "Clique na parte que pretende alterar ou elimine a notificação selecionada",
    "edNotif2": "Selecione o meio de transporte",
    "edNotif3": "Selecione o local de embarque e o intervalo de tempo com que deseja ser notificado",
    "edNotif4": "Introduza a data de embarque",
	"delNotification": "Confirme ou cancele a eliminação da notificação selecionada",
    "payments": "Selecione o cartão para efetuar o pagamento, elimine-o pressionando sobre ele ou adicione um novo",
    "cardNumber": "Escreva o número do cartão utilizando o teclado abaixo",
    "cardVal": "Introduza a validade do cartão",
    "confirmCard": "Confirme ou cancele a criação do cartão",
	"pay1": "Encoste o dispositvo ao terminal de pagamento ou troque de cartão",
	"delCard": "Confirme ou cancele a eliminação do cartão selecionado",
    "pi": "Selecione um ponto de interesse, elimine-o pressionando sobre ele ou adicione um novo",
    "piCategories": "Selecione a categoria do ponto de interesse",
    "piClosePlaces": "Selecione o ponto de interesse próximo que pretende adicionar",
    "piPath": "Siga as direções indicadas pelo seu iGo ou compre entradas, caso a opção esteja disponível",
    "piBuy": "Selecione o número de entradas a comprar",
	"confirm10Plus": "Como medida de segurança, confirme que pretende comprar mais do que 10 entradas",
    "piCard": "Selecione um cartão para pagar ou adicione um novo",
	"pay1PI": "Confirme a compra das entradas para o ponto de interesse selecionado",
	"piRemove": "Confirme ou cancele a eliminação do ponto de interesse selecionado"
}

var Cards = [];
var Notifications = [];
var MyPI = []; 
