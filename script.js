// Correção do bug do módulo (%) de Javascript
Number.prototype.mod = function(n) {
    return ((this % n) + n) % n;
};

// Meses e dias para a data
const months = ["jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago", "set", "out", "nov", "dez"];
const days = ["Dom", "Seg", "Ter", "Qua", "Qui", "Sex", "Sáb"];

// iGo bloqueado ou não
var locked = true;

// Número de cliques (para duplo clique
var clicks = 0;

// Lista de relógios para atualizar
var clocks;

// Ecrã ativo atualmente
var active;
// Ecrã desligado
var offscreen;
// Ecrã de bloqueio
var lockscreen;
// Ecrã de Ajuda
var helpscreen;
// Ecrã inicial
var home;
// Ecrã de notificações
var notifications;
// Led de notificações
var LED;

/* Obter informações ao carregar */
window.onload = function() {
    // Obter ecrãs e relógios
    offscreen = document.getElementById("offscreen");
    lockscreen = document.getElementById("lockscreen");
    helpscreen = document.getElementById("helpscreen");
    active = home = document.getElementById("home");
    notifications = document.getElementById("notifications");
    clocks = document.getElementsByClassName("clock");

    /* Slide no ecrã de bloqueio */
    var lsHam = new Hammer(document.getElementById("lockscreen"));
    lsHam.on("panup pandown panleft panright", function(event) {
        lockscreen.style.display = "none";
        active.style.display = "inline-block";
    });

    /* Slide para o ecrã de notificações */
    var notAccess = document.getElementsByClassName("notAccess");
    for (let i = 0; i < notAccess.length; i++) {
        var notHam = new Hammer(notAccess[i]);
        notHam.on("pandown", function(event) {
            active.style.display = "none";
            notifications.style.display = "inline-block";
			stopLight();
        });
    }

    /* Slide para sair do ecrã de notificações */
    var notAccess = new Hammer(notifications);
    notAccess.on("panup", function(event) {
        notifications.style.display = "none";
        active.style.display = "inline-block";
    });

    /* Funcionalidades do botão de bloqueio */
    document.getElementById("LCKBtn").addEventListener('click', function() {
        clicks++;
        // Definir se é duplo clique ou não
        if (clicks == 1) {
            timer = setTimeout(function() {
                clicks = 0;
                lockClick();
            }, 200);
        } else {
            clearTimeout(timer);
            clicks = 0;
            goHome();
        }
    });

    /* Funcionalidades do botao de ajuda */

    document.getElementById("HLPBtn").addEventListener('click', function() {

        if (helpscreen.style.display != "none") {
            active.style.display = "inline-block";
            helpscreen.style.display = "none";
        } else if (!locked && lockscreen.style.display == "none" && helpTexts[active.id] != undefined) {
            active.style.display = "none";
            helpscreen.style.display = "inline-block";

			document.getElementById("helpText").innerHTML = helpTexts[active.id];
        }

    });

    updateAllNotif();
    checkNotify();
    updateDate();
}


/* Luz a piscar */
async function blinkLight(){
	LED = undefined;
	while(LED == undefined){
		document.getElementById("notifLED").style.backgroundColor = "#0a9e00";
		await sleep(1000);
		document.getElementById("notifLED").style.backgroundColor = "grey";
		await sleep(1000);
	}
}

function stopLight(){
	if(LED == undefined){
		clearTimeout(LED);
		document.getElementById("notifLED").style.backgroundColor = "grey";
		LED = true;
	}
}

/* Atualizar data e hora */
function updateDate() {
    var date = new Date();
    var hours = digitCheck(date.getHours());
    var minutes = digitCheck(date.getMinutes());

    // Atualizar relógios
    for (let i = 0; i < clocks.length; i++) {
        clocks[i].innerHTML = hours + ":" + minutes;
    }

    // Atualizar data
    document.getElementById("date").innerHTML = days[date.getDay()] + ", " + date.getDate() + " de " + months[date.getMonth()];

    setTimeout(updateDate, (60 - date.getSeconds()) * 1000);
}

function digitCheck(i) {
    if (i < 10) {
        i = "0" + i;
    }

    return i;
}

/* Obter tempo */
function getTime() {
    var time = new Date();

    return digitCheck(time.getHours()) + ":" + digitCheck(time.getMinutes());
}

/* Botão de bloqueio (1 clique) */
function lockClick() {
    if (locked) {
        lockscreen.style.display = "inline-block";
        offscreen.style.display = "none";
        locked = false;
    } else {
        active.style.display = "none";
        lockscreen.style.display = "none";
        notifications.style.display = "none";
		helpscreen.style.display = "none";
        offscreen.style.display = "inline-block";
        locked = true;
    }
}

/* Mudar de ecrã */
function changeScreen(screen) {
    active.style.display = "none";
    active = document.getElementById(screen);
    active.style.display = "inline-block";
}

/* Ir para o ecrã inicial */
function goHome() {
    if (!locked && lockscreen.style.display == "none") {
		simulMovOFF();
		simulCardOFF();
		document.getElementById("simulPay").style.display = "none";
		helpscreen.style.display = "none";
        changeScreen("home");
    }
}

/* Função de sleep */
function sleep(ms) {
    return new Promise(resolve => setTimeout(resolve, ms));
}

/* Gerador de IDs */
function makeID(length) {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";

    for (var i = 0; i < length; i++)
        text += possible.charAt(Math.floor(Math.random() * possible.length));

    return text;
}
